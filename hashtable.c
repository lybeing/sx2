#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<time.h>
#include"hashtable.h"

#define MAX_BUCKETS 100
#define MULTIPLIER 31

#define LENGTH 30
typedef struct User_login{
    char name[LENGTH];
    int totalcount;
}ELementType;

typedef struct SListNode
{
    ELementType data;
    struct SListNode* next;
}Node,*PNode,*List;

static PNode table[MAX_BUCKETS];

static unsigned long hashstring(const char*str);
static void cleartable();
static PNode walloc(const char *str);
static PNode lookup(const char *str);
static PNode find(PNode wp,const char *str);

void InitTable();
void Insert(PNode wp,unsigned long key,const char*name);

static PNode walloc(const char *str)
{
    PNode p = (PNode)malloc(sizeof(Node));
    if(p != NULL)
    {
        strcpy(p->data.name,str);
        p->data.totalcount = 0;
        p->next = NULL;
    }
    return p;
}

static unsigned long hashstring(const char *str)
{
    unsigned long h=0,result = 0;
    while(*str)
    {
        h = h*MULTIPLIER +  *str;
        str++;
    } 
    result = h % MAX_BUCKETS;
    return result;
}

void InitTable()
{
    for(int i=0;i<MAX_BUCKETS;i++)
    {
        table[i] = (Node*)malloc(sizeof(Node));
        table[i]->next = NULL;
    }
}

static PNode find(PNode wp,const char *str)
{
    wp = table[hashstring(str)];
    while(wp)
    {
        if(strcmp(wp->data.name,str)==0)
        return wp;
        wp = wp->next;
    }
    return NULL;
}

void Insert (PNode wp,unsigned long key,const char*name)
{
    Node *p;
    p = find(wp,name);
    if(p!=NULL)
    p->data.totalcount++;
    else{
        Node *q = wp;
        q->next = table[key];
        table[key] = q;
    }
}

static PNode lookup(const char *str)
{
    unsigned long h = hashstring(str);
    PNode p = NULL,wp = table[h];
    for (p = wp;p!=NULL;p = p->next)
    {
        if(strcmp(p->data.name,str)==0)
        return p;
    }
    p = walloc(str);
    return p;
}

static void cleartable()
{
    PNode wp = NULL,p = NULL;
    int i = 0;
    for (i=0;i<MAX_BUCKETS;i++)
    {
        wp = table[i];
        while(wp)
        {
            p =wp;
            wp = wp->next;
            free(p);
        }
    }
}

void file_read_ht()
{
    FILE*fp = fopen("user_login.txt","r");
    char word[1624];
    char *name;
    PNode wp = NULL;
    memset (table,0,sizeof(table));
    int count = 0;
    while(1)
    {
        if(fscanf(fp,"%s",word)!=1)
        break;
        name = strtok(word,",");
        unsigned long key = hashstring(name);
        wp = walloc(name);
        Insert(wp,key,name);
        count++;
    }
    printf("%d\n",count);
    fclose(fp);
}

void file_write_ht()
{
    FILE*fp;
    int count = 0;
    if((fp=fopen("output.txt","wt"))==NULL)
    {
        printf("Fail to open file!\n");
        exit(0);
    }
    if(fp==NULL)
    return ;
    for(int i=0;i<MAX_BUCKETS;i++)
    {
        Node*p = table[i];
        while(p!=NULL)
        {
            fprintf(fp,"%s %d\n",p->data.name,p->data.totalcount);
            p = p-> next;
            count++;
        }
    }
    printf("%d\n",count);
    fclose(fp);
}

void search_ht()
{
    char name[LENGTH];
    printf("Enter name,'q' to exit:\n");
    scanf("%s",name);
    int flag = 0;
    while(strcmp(name,"q"))
    {
        unsigned long hash = hashstring(name);
        PNode wp = table[hash];
        PNode p  = NULL;

        for(p=wp;p!=NULL;p = p->next)
        {
            if(strcmp(p->data.name,name)==0)
            {
                printf("%s,%d\n",p->data.name,p->data.totalcount);
                flag = 1;
                break;
            }
        }
        if(flag==0)
        printf("NULL\n");
        scanf("%s",name);
    }
    cleartable;
}

int main()
{
    clock_t start,finish;
    double duration;
    start = clock();
    file_read_ht();
    file_write_ht();
    finish = clock();
    duration = (double)(finish - start);
    printf("%f seconds\n",duration);
    search_ht();
    return 0;
}
