#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <string.h>

#define BUFFER_SIZE 1024 * 1024

struct thread_args
{
    int fdr;
    int fdw;
};

void *read_file(void *args)
{
    struct thread_args *targs = (struct thread_args *)args;
    char buffer[BUFFER_SIZE];
    ssize_t bytes_read;

    while ((bytes_read = read(targs->fdr, buffer, BUFFER_SIZE)) > 0)
    {
        write(targs->fdw, buffer, bytes_read);
    }
    return NULL;
}

int compare_files(const char* file1, const char* file2) {
    FILE* f1 = fopen(file1, "rb");
    FILE* f2 = fopen(file2, "rb");

    if (f1 == NULL || f2 == NULL) {
        printf("无法打开文件\n");
        return 0;
    }

    int byte1, byte2, pos = 0;
    int files_identical = 1;

    while ((byte1 = fgetc(f1)) != EOF && (byte2 = fgetc(f2)) != EOF) {
        pos++;

        if (byte1 != byte2) {
            printf("文件内容不一致，第 %d 字节不同\n", pos);
            files_identical = 0;
            break;
        }
    }

    if (byte1 != byte2) {
        printf("文件长度不一致\n");
        files_identical = 0;
    } else if (files_identical) {
        printf("文件内容完全相同\n");
    }

    fclose(f1);
    fclose(f2);

    return files_identical;
}


int main(int argc, char **argv)
{
    int fdr, fdw;
    struct timeval start_time, end_time;
    double time;
    pthread_t tid;
    struct thread_args targs;

    if (argc != 3)
    {
        printf("Usage: %s srcfilename decfilename\n", argv[0]);
        return -1;
    }

    gettimeofday(&start_time, NULL);

    fdr = open(argv[1], O_RDONLY);
    if (fdr < 0)
    {
        perror("Failed to open source file");
        return -1;
    }

    fdw = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (fdw < 0)
    {
        perror("Failed to open destination file");
        return -1;
    }

    targs.fdr = fdr;
    targs.fdw = fdw;

    if (pthread_create(&tid, NULL, read_file, (void *)&targs) != 0)
    {
        perror("Failed to create thread");
        return -1;
    }

    if (pthread_join(tid, NULL) != 0)
    {
        perror("Failed to join thread");
        return -1;
    }

    close(fdr);
    close(fdw);

    gettimeofday(&end_time, NULL);
    time = (end_time.tv_sec - start_time.tv_sec) * 1000000;
    time = time + (end_time.tv_usec - start_time.tv_usec);
    time = time / 1000000;
    printf("Use time = %lf s done\n", time);

    // 验证生成的文件是否与源文件完全相同
    if (compare_files(argv[1], argv[2]) == 1)
    {
        printf("File copy successful! The copied file is identical to the source file.\n");
    }
    else
    {
        printf("File copy failed! The copied file is different from the source file.\n");
    }

    return 0;
}
