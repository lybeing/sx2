#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "head.h"

void insertNode(Node** head, char* username) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    strcpy(newNode->username, username);
    newNode->login_count = 1;
    newNode->next = NULL;

    if (*head == NULL) {
        *head = newNode;
    } else {
        Node* current = *head;
        while (current->next != NULL) {
            if (strcmp(current->username, username) == 0) {
                current->login_count++;
                return;
            }
            current = current->next;
        }
        current->next = newNode;
    }
}

void mergeNodes(Node* head) {
    Node* current = head;
    while (current != NULL) {
        Node* temp = current->next;
        Node* prev = current;
        while (temp != NULL) {
            if (strcmp(current->username, temp->username) == 0) {
                current->login_count += temp->login_count;
                prev->next = temp->next;
                free(temp);
                temp = prev;
            }
            prev = temp;
            temp = temp->next;
        }
        current = current->next;
    }
}

void printList(Node* head) {
    Node* current = head;
    while (current != NULL) {
        printf("%s,%d\n", current->username, current->login_count);
        current = current->next;
    }
}

void freeList(Node** head) {
    Node* current = *head;
    while (current != NULL) {
        Node* temp = current;
        current = current->next;
        free(temp);
    }
    *head = NULL;
}
