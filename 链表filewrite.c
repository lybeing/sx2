#include <stdio.h>
#include <stdlib.h>
#include "head.h"

void writeToFile(Node* head, char* filepath) {
    FILE* file = fopen(filepath, "w");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    Node* current = head;
    while (current != NULL) {
        fprintf(file, "%s,%d\n", current->username, current->login_count);
        current = current->next;
    }

    fclose(file);
}
