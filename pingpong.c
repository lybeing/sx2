#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

int main(int argc, char **argv)
{
    //无名管道为半双工通道：可以接受和发送信息，但在同一时间只能接受或发送信息
    int fd1[2];	//父进程向子进程发送通道
    int fd2[2];	//子进程向父进程发送通道

    pipe(fd1);	//创建管道
    pipe(fd2);	//创建管道

    if(fork() == 0)	//子进程
    {
        close(fd1[1]);	//该通道子进程只读，关闭写端，下同
        close(fd2[0]);	//该通道子进程只写，关闭读端，下同

        char buf[32];	

        int pi = read(fd1[0], buf, 32 * sizeof(char));	//读入最多32字节的数据，返回值为实际读到数据大小，下同

        buf[pi] = '\0';	//置字符串末尾下一个字符为'\0'，便于打印

        printf("%d: received %s\n", getpid(), buf);
        

        write(fd2[1], "pong", 4 * sizeof(char));	//写入"pong"字符串，大小为4 * sizeof(char)
        exit(0);
    }
    else		//父进程
    {
        close(fd1[0]);
        close(fd2[1]);

        write(fd1[1], "ping", 4 * sizeof(char));

        char buf[32];
        int pi = read(fd2[0], buf, 32 * sizeof(char));

        buf[pi] = '\0';

        printf("%d: received %s\n", getpid(), buf);

        wait(0);	//等待子进程结束
        exit(0);
    }

}
