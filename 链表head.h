#ifndef HEAD_H
#define HEAD_H

typedef struct Node {
    char username[50];
    int login_count;
    struct Node* next;
} Node;

void insertNode(Node** head, char* username);
void mergeNodes(Node* head);
void printList(Node* head);
void freeList(Node** head);

#endif
