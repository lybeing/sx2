#include <stdio.h>
#include "head.h"

int main() {
    Node* head = NULL;
    char filepath[] = "user_login.txt";

    readFromFile(&head, filepath);
    mergeNodes(head);
    writeToFile(head, "result.txt");
    printList(head);
    freeList(&head);

    return 0;
}
