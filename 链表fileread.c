#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "head.h"

void readFromFile(Node** head, char* filepath) {
    FILE* file = fopen(filepath, "r");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char line[100];
    char *username, *date;
    char delim[] = ",";

    while (fgets(line, 100, file) != NULL) {
        username = strtok(line, delim);
        date = strtok(NULL, delim);

        insertNode(head, username);
    }

    fclose(file);
}
